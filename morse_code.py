import subprocess
import time


LETTERS = {
    ' ': '_',
    'A': '.-',
    'B': '-...',
    'C': '-.-.',
    'D': '-..',
    'E': '.',
    'F': '..-.',
    'G': '--.',
    'H': '....',
    'I': '..',
    'J': '.---',
    'K': '-.-',
    'L': '.-..',
    'M': '--',
    'N': '-.',
    'O': '---',
    'P': '.--.',
    'Q': '--.-',
    'R': '.-.',
    'S': '...',
    'T': '-',
    'U': '..-',
    'V': '...-',
    'W': '.--',
    'X': '-..-',
    'Y': '-.--',
    'Z': '--..',
    '0': '-----',
    '1': '.----',
    '2': '..---',
    '3': '...--',
    '4': '....-',
    '5': '.....',
    '6': '-....',
    '7': '--...',
    '8': '---..',
    '9': '----.',
    '&': '.-...',
    "'": '.----.',
    '@': '.--.-.',
    ')': '-.--.-',
    '(': '-.--.',
    ':': '---...',
    ',': '--..--',
    '=': '-...-',
    '!': '-.-.--',
    '.': '.-.-.-',
    '-': '-....-',
    '+': '.-.-.',
    '"': '.-..-.',
    '?': '..--..',
    '/': '-..-.'
}
UNIT = 0.05
LONG = 3
CHAR_SPACE = 3
WORD_SPACE = 7
PITCH = 1000


def playsound(length, pitch=PITCH):
    subprocess.call(['play', '-q', '-n', 'synth', str(length), 'sin', str(pitch)])
    
def playletter(letter, unit=UNIT):
    code = LETTERS.get(letter, '')
    for c in code:
        if c == '.':
            playsound(unit)
        elif c == '-':
            playsound(unit * LONG)
        elif c == '_':
            time.sleep(unit * (WORD_SPACE - 1))
        time.sleep(UNIT)
    time.sleep(unit * (CHAR_SPACE - 1))
    
def morse(text, **kwargs):
    for c in text.upper().strip().replace('  ', ' '):
        playletter(c, **kwargs)


